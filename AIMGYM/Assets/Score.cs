using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private Vector3 targetpos;
    public int score = 0;
    public Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score : " + score;
        targetpos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Sin(Time.time) * 10.0f+targetpos.x,targetpos.y,targetpos.z);
    }

    void OnCollisionEnter(Collision other)
    {
        score += 10;
        scoreText.text = "Score : " + score;
    }

}
